//Задание 1: prompt/alert

function checkStringValues (name){
  while (/[^А-Яа-яA-Za-z\-\s]/.test(name))
    name = prompt("Enter again:", "value");
  return name;
}

function checkNumberValue (number){
  while (/[^0-9]/.test(number))
    number = prompt('Enter again:', '20');
  return number;
}

function AskPersonalData() {
  let studentLastName = prompt('What is your last name?');
  studentLastName = checkStringValues(studentLastName.trim());

  let studentFirstName = prompt('What is your first name?');
  studentFirstName = checkStringValues(studentFirstName.trim());

  let studentAge = prompt('How old are you?');
  studentAge = checkNumberValue(studentAge.trim());

  let university = prompt('Where do you study?');
  university = checkStringValues(university.trim());

  let course = prompt("Enter your course:");
  course = checkNumberValue(course.trim());

  let hasDebt = confirm("Do you have some academical debts?");
  
  alert('Last name: ' + studentLastName + '\nFirst name: ' + studentFirstName +
        '\nAge: ' + studentAge + '\nUniversity: ' + university +
        '\nCourse: ' + course + '\nAcademical debts: ' + hasDebt);
  
  return {'lastName': studentLastName, 'firstName': studentFirstName, 'age': studentAge, 
          'university': university, 'course': course, 'debts': hasDebt};
}

AskPersonalData();

//Задание 2: квадратное уравнение
function solveEquation(a, b, c) {
    let result = [];
    let D = b * b - 4 * a * c;
    if (D >= 0) {
        result[0] = (-b + Math.sqrt(D)) / (2 * a);
        result[1] = (-b - Math.sqrt(D)) / (2 * a);
        alert(`${a}*x^2 + ${b}*x + ${c}:\nSolution is: ${result}`);
    } else {
        alert(`${a}*x^2 + ${b}*x + ${c}:\nNo solution`);
    }
    return result;
}
solveEquation(1, 6, 9);
solveEquation(1, 8, 5);
solveEquation(12, -15, 10);

// Задание 3: Посчитать сумму цифр
function getSum(num) {
    if (num < 1) return 0;
    else return num % 10 + getSum(parseInt(num / 10));
}

alert('123: ' + getSum(123));
alert('8013 :' + getSum(8013));
