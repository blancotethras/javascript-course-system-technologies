/* 1. Функция принимает массив полей и "фильтрующий объект", возвращает уникальные объекты:

		function unique(array, filterObj) {...}

		arr = [{a: 1, b: 2, c:1}, {a:1, b:1}, {a:2}, {a:1, b:2}];

		filterObj:

		['a'] - возвратит первый и третий объект
		['b'] - вернет первый, третий и четвертый
		['a','b'] - первый, второй, третий
		['f'] - вернется только первый, потому что f - undefined 
*/

function isUnique (obj, filter, filteredArr) {
	let counter = 0;
	let propertyExists = false;
	for (let j = 0; j < filter.length; j++) {
		if (obj.hasOwnProperty(filter[j])) {
			for (let k = 0; k < filteredArr.length; k++)
				if (filteredArr[k].hasOwnProperty(filter[j])) {
					if (Object.getOwnPropertyDescriptor(obj, filter[j]).value === Object.getOwnPropertyDescriptor(filteredArr[k], filter[j]).value) {
						counter++;
					}
					propertyExists = true;
				} 
		}
		if (!propertyExists) return false;
		if (counter === filter.length){
		return false;
	} else {
		return true;
	}
	}
}

function getUniqueObjects(arr, filter) {
	if (filter === undefined || filter.length === 0 || !filter) {
		return arr;
	}
	let filteredArr = [];
	if (arr.length > 0) {
		filteredArr.push(arr[0]);
	}
	for (let i = 1; i < arr.length; i++) {
		if (isUnique(arr[i], filter, filteredArr)) {
			filteredArr.push(arr[i]);
		}
	}
	return filteredArr;
}

// 2. Найти третий наименьший элемент массива наиболее оптимальным способом

function getUniqueElements(arr) {
  var obj = {};
  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true;
  }
  return Object.keys(obj);
}

function findThirdLeastElement(arr) {
	arr = arr.filter((a) => typeof a === 'number' ? true : false);
	arr = getUniqueElements(arr);
	if (arr.length >= 3) {
		arr.sort((a, b) => a - b);
		return arr[2];
	} else {
		return undefined;
	}
}