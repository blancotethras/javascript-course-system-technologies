class myStorage {
    set(key, value) {
        localStorage.setItem(key, value);
        return localStorage;
    }

    add(...objects) {
        for (let i = 0; i < objects.length; i++) {
            this.set(Object.keys(objects[i])[0], objects[i][Object.keys(objects[i])[0]]);
        }
        return localStorage;
    }

    get(key) {
        return localStorage.getItem(key);
    }

    getAll() {
        let storageArray = [];
        for (let i = 0; i < localStorage.length; i++) {
            let prop = {};
            Object.defineProperty(prop, Object.entries(localStorage)[i][0], {value: Object.entries(localStorage)[i][1]})
            storageArray.push(prop);
        }
        return storageArray;
    }

    remove(key) {
        localStorage.removeItem(key);
        return localStorage;
    }

    clear() {
        localStorage.clear();
        return localStorage;
    }
}

let s = new myStorage();
